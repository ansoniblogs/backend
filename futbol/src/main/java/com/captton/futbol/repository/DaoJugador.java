package com.captton.futbol.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.futbol.model.Club;
import com.captton.futbol.model.Jugador;

public interface DaoJugador extends JpaRepository<Jugador, Long> {

	public List<Jugador> findByClub(Club club);
}
