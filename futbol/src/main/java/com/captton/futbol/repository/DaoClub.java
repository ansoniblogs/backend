package com.captton.futbol.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.futbol.model.Club;

public interface DaoClub extends JpaRepository<Club, Long> {

}
