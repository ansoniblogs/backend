package com.captton.futbol.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="Jugadores")
public class Jugador {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String nombre;
	private String edad;
	private String foto;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "club_id")
		private Club club;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getFoto() {
		return foto;
	}

	public Club getClub() {
		return club;
	}

	public void setClub(Club club) {
		this.club = club;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	public void asignarClub(Club club) {
		this.setClub(club);
	}
	

}
