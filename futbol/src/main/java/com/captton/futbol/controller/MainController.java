package com.captton.futbol.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.captton.futbol.model.Club;
import com.captton.futbol.model.InputCambioClub;
import com.captton.futbol.model.Jugador;
import com.captton.futbol.repository.DaoClub;
import com.captton.futbol.repository.DaoJugador;


@RestController
@RequestMapping({"/futbol"})
public class MainController {

	@Autowired
	private DaoClub clubdao;
	@Autowired
	private DaoJugador jugadordao;

	//API Dar de alta un club.
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(path= {"/club"})
	public Club create(@RequestBody Club club) {
		return clubdao.save(club);
	}
	
	//API Dar de alta un jugador.
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(path= {"/jugador"})
	public Jugador create(@RequestBody Jugador jugador) {
		return jugadordao.save(jugador);
	}
	
	//API Get de todos los jugadores de un club (con su ID). Si uso un ID que no es de un club, me tira todos los players.
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(path= {"/jugador/{id_club}"})
	public ResponseEntity<Object> listarJugadoresDeClub(@PathVariable Long id_club){
		
		Club auxclub = clubdao.findById(id_club).orElse(null);
		List<Jugador> jugadoresFound = jugadordao.findByClub(auxclub);
		JSONObject obj = new JSONObject();
		
		if(!jugadoresFound.isEmpty()) {
			
			JSONArray jugArray = new JSONArray();
			for(Jugador auxj: jugadoresFound) {
				JSONObject aux = new JSONObject();
				aux.put("nombre", auxj.getNombre());
				aux.put("foto", auxj.getFoto());
				aux.put("edad", auxj.getEdad());
				
				jugArray.put(aux);
				
				obj.put("error", 0);
				obj.put("jugadores", jugArray);
				
			}
			
		}else {
			obj.put("error", 1);
			obj.put("message", "El club no posee jugadores.");
		}
		
		
		
		return ResponseEntity.ok().body(obj.toString());
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(path= {"/jugadores"})
	public ResponseEntity<Object> listarJugadores(){
		
		Club auxclub = null;
		List<Jugador> jugadoresFound = jugadordao.findByClub(auxclub);
		JSONObject obj = new JSONObject();
		
		if(!jugadoresFound.isEmpty()) {
			
			JSONArray jugArray = new JSONArray();
			for(Jugador auxj: jugadoresFound) {
				JSONObject aux = new JSONObject();
				aux.put("nombre", auxj.getNombre());
				aux.put("foto", auxj.getFoto());
				aux.put("edad", auxj.getEdad());
				aux.put("id", auxj.getId());
				
				jugArray.put(aux);
				
				obj.put("error", 0);
				obj.put("jugadores", jugArray);
				
			}
			
		}else {
			obj.put("error", 1);
			obj.put("message", "El club no posee jugadores.");
		}
		
		
		
		return ResponseEntity.ok().body(obj.toString());
	}

	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(path= {"/clubes"})
	public ResponseEntity<Object> listarClubes(){
		
		List<Club> listaclubes = clubdao.findAll();
		//Club auxclub = clubdao.findById(id_club).orElse(null);
		JSONObject obj = new JSONObject();
		
		if(!listaclubes.isEmpty()) {
			
			JSONArray clubArray = new JSONArray();
			for(Club clu: listaclubes) {
				JSONObject aux = new JSONObject();
				aux.put("nombre", clu.getNombre());
				aux.put("foto", clu.getLogo());
				aux.put("password", clu.getPassword());
				aux.put("id", clu.getId());
				
				clubArray.put(aux);
				
				obj.put("error", 0);
				obj.put("clubes", clubArray);
				
			}
			
		}else {
			obj.put("error", 1);
			obj.put("message", "No hay clubes");
		}
		
		
		
		return ResponseEntity.ok().body(obj.toString());
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(path= {"/jugadores"})
	public ResponseEntity<Object> ficharJugador(@RequestBody InputCambioClub input){
		JSONObject obj = new JSONObject();
		Jugador jugadorEnc = jugadordao.findById(input.getIdPlayer()).orElse(null);
		Club clubEnc = clubdao.findById(input.getIdClub()).orElse(null);
		
		if(jugadorEnc!=null) {
		//	System.out.println(jugadorEnc.getId());
			jugadorEnc.asignarClub(clubEnc);
			jugadordao.save(jugadorEnc);
			
			obj.put("error", 0);
			obj.put("message", "Se ha asignado el club al jugador");
		}else {
			obj.put("error", 1);
			obj.put("message", "No se encontro el jugador con esa id");
		}
		
		
		return ResponseEntity.ok().body(obj.toString());
	}
	
	
}
